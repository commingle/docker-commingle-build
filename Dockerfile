FROM rustlang/rust:nightly-buster-slim

RUN apt-get update && \
      apt-get install -yq build-essential curl pkg-config libssl-dev libpq-dev chromium libgbm1 python3 && \
      curl https://get.volta.sh | bash && . ~/.bashrc && volta install node@14 && \
      cargo install --git https://github.com/holmgr/cargo-sweep.git --rev 0ffc0419f1b9afcb2ca7f5aa5671b732e8aed7cd cargo-sweep
